<?php

$link = mysqli_connect("mariadb", "root", "password", "mydb");

if ($link) {
  echo "Connection to mariadb is okay" . PHP_EOL;
  exit;
}

if (!$link) {
    echo "Error connection is failed." . PHP_EOL;
    echo "Error Code errno: " . mysqli_connect_errno() . PHP_EOL;
    echo "Eror Text error: " . mysqli_connect_error() . PHP_EOL;
    exit;
}
?>