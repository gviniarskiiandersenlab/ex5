### Demo ci/cd to kubernetes cluster

#### How to setup:
1. Setup k8s cluster (gcp for example)
2. Deploy Gitlab runner to kubernetes (optional)
3. Add new variables to gitlab settings: 
  * KUBECONFIG (as file)
  * CI_REGISTRY_USER
  * CI_REGISTRY_TOKEN
4. Add database manually:
```
k run --image=mariadb:10.5.9 mariadb --port=3306 --env="MYSQL_ROOT_PASSWORD=password"
k expose po mariadb --port=3306 --name=mariadb
k exec mariadb -- mysql -uroot -ppassword -e "CREATE DATABASE mydb"
```
5. Execute pipeline
6. Check application in load balancer IP/Hostname. http://34.117.145.10

#### Known problems:
1. Not solved problem with kaniko docker image caching, and build+release jobs was merged;
2. Mostly used hardcoded parameters for fast deploy and check;
3. Docker processes started by root user and required secure improvents for specific stack.